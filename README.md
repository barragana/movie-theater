# My Movie Theater Application

## Demo
Click [here](https://my-movie-theater.herokuapp.com/)

Heroku Dynos might be asleep, so it might take a while to load.

## Setup
1. Clone repo
2. `yarn install`
3. Create your onw API Key of themoviedb.org
4. Create `.env.local` file and past your API_KEY like bellow
```
REACT_APP_TOKEN_V3=8d681982d..........2ab3159fd3673
REACT_APP_TOKEN_V4=eyJhbGciOiJIUzI1XXXX.eyJhdWQiOiI4ZDY4MTk4MmRmYmE4MjA1M2ZjMmFXXXXXXXXXXzY3MyIsInN1YiI6IjVlOWIwNjAxMzEwMzI1MDAyMWM2ZjgwMiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.906WUeIIGsEkXweT8SpnMAf03Lk-XXXXXXXXXXAVEjk
```

## Running
1. `yarn start` -> localhost:3000

## How it works
1. It always display the Popular Movies section sorted by popularity desc
2. You can search Movies and they will be displayed right bellow into the Results section
3. There is also the possibility to filter movies by stars(`vote_average`), however search and filter work in different APIs. Which means, if you apply stars filter, it will not apply on top of searched results and that is why search input gets cleaned when you click on stars
4. If you click on any Poster you go to the Movie page details
5. You can access directly the Movie page details

## Components
### Styled Components
By [`styled-components`](styled-components.com) we get a better encapsulation, avoid writing multiple files `.js` and `.css`, get easier, more javascript way to share and extends components making it easier to see the style organization.

## Redux
### Ducks
By using [`ducks`](https://github.com/erikras/ducks-modular-redux) approach we avoid also to write many files for example: `constants.js`, `actions.js` and `reducers`. As usually they are not big and has same relation to each it invision that would make sense to group them in a single file, making navigation and accessibility easier.

### Saga
Despite that one of the main usability of [`redux-saga`](https://redux-saga.js.org/) is to handle asynchronous side effects like data fetching and impure things like accessing the browser cache, it also can be used to handle side effects of different reducers/store. Helping to separate some business logic from reducers. In our case, it handles side effects caused by `apps` to `servers` and vice-versa. So, `clurster` is responsible for applying the coordination logic then trigger actions to update store accordingly.

### Selectors
By using selectors we extract some data we need from store easier. Also, we could memoized those selections and computations in case they don't change by using [`re-select`](https://github.com/reduxjs/reselect), improving performance by avoid re-computing selectors that does not change.

### Immutable
By using [`Immutable`](https://facebook.github.io/immutable-js/) we guarantee that state will be always an immutable object. It also facilitates some manipulation with arrays, object, etc once we get used to it.

# Improvements
1. I would either have a api that supports filter and search in order to have stars filter applied on top of searched movies
2. Integrate search input with url query to support loading directly with search value
3. Integrate filter stars with url query to support loading directly with filter value
4. Add View All where it is possible to filter, search and paginate the results
6. Add top level of integration test. I commonly use Cypress to cover the whole flow
7. Add second level of integration tests between `actions` -> `saga` -> `store`. A kind of test that validates the whole flow, by taking a real(mocked) store as based and computating over it.
8. Add components tests with [`jest`](https://jestjs.io/) and [`enzyme`](https://airbnb.io/enzyme/docs/api/)
