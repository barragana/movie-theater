import { fromJS } from 'immutable';

export const FETCH_POPULAR_MOVIES_REQUESTED = 'FETCH_POPULAR_MOVIES_REQUESTED';
export const fetchPopularMoviesRequested = () => ({
  type: FETCH_POPULAR_MOVIES_REQUESTED,
});

export const FETCH_POPULAR_MOVIES_SUCCESSED = 'FETCH_POPULAR_MOVIES_SUCCESSED';
export const fetchPopularMoviesSuccessed = (movies) => ({
  type: FETCH_POPULAR_MOVIES_SUCCESSED,
  payload: { movies },
});

export const FETCH_POPULAR_MOVIES_FAILED = 'FETCH_POPULAR_MOVIES_FAILED';
export const fetchPopularMoviesFailed = (error) => ({
  type: FETCH_POPULAR_MOVIES_FAILED,
  payload: { error },
});

export const initialState = fromJS({
  isFetching: false,
  didInvalidate: false,
  pagination: {
    isFetching: false,
    didInvalidate: false,
    page: 1,
    error: '',
  },
  error: '',
  data: [],
});

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_POPULAR_MOVIES_REQUESTED:
      return state.set('isFetching', true).set('didInvalidate', false);

    case FETCH_POPULAR_MOVIES_SUCCESSED:
      return state.set('isFetching', false).set('data', fromJS(payload.movies));

    case FETCH_POPULAR_MOVIES_FAILED:
      return state
        .set('isFetching', false)
        .set('didInvalidate', true)
        .set('error', payload.error);
    default:
      return state;
  }
};
