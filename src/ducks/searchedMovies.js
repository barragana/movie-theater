import { fromJS } from 'immutable';

export const SEARCH_MOVIES_REQUESTED = 'SEARCH_MOVIES_REQUESTED';
export const searchMoviesRequested = (term) => ({
  type: SEARCH_MOVIES_REQUESTED,
  payload: { term },
});

export const FILTER_MOVIES_REQUESTED = 'FILTER_MOVIES_REQUESTED';
export const filterMoviesRequested = (stars) => ({
  type: FILTER_MOVIES_REQUESTED,
  payload: { stars },
});

export const SEARCH_MOVIES_SUCCESSED = 'SEARCH_MOVIES_SUCCESSED';
export const searchMoviesSuccessed = (movies) => ({
  type: SEARCH_MOVIES_SUCCESSED,
  payload: { movies },
});

export const SEARCH_MOVIES_FAILED = 'SEARCH_MOVIES_FAILED';
export const searchMoviesFailed = (error) => ({
  type: SEARCH_MOVIES_FAILED,
  payload: { error },
});

export const initialState = fromJS({
  isFetching: false,
  didInvalidate: false,
  pagination: {
    isFetching: false,
    didInvalidate: false,
    page: 1,
    error: '',
  },
  error: '',
  data: [],
  term: '',
  stars: 0,
});

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FILTER_MOVIES_REQUESTED:
      return state
        .set('isFetching', true)
        .set('term', '')
        .set('stars', payload.stars * 2)
        .set('didInvalidate', false);

    case SEARCH_MOVIES_REQUESTED:
      return state
        .set('isFetching', true)
        .set('term', payload.term)
        .set('stars', 0)
        .set('didInvalidate', false);

    case SEARCH_MOVIES_SUCCESSED:
      return state.set('isFetching', false).set('data', fromJS(payload.movies));

    case SEARCH_MOVIES_FAILED:
      return state
        .set('isFetching', false)
        .set('didInvalidate', true)
        .set('error', payload.error);
    default:
      return state;
  }
};
