import { combineReducers } from 'redux';

import popularMovies from './popularMovies';
import movie from './movie';
import searchedMovies from './searchedMovies';

export default () => combineReducers({
  popularMovies,
  movie,
  searchedMovies,
});
