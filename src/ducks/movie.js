import { fromJS } from 'immutable';

export const SELECT_MOVIE = 'SELECT_MOVIE';
export const selectMovie = (movie) => ({
  type: SELECT_MOVIE,
  payload: { movie },
});

export const FETCH_MOVIE_REQUESTED = 'FETCH_MOVIE_REQUESTED';
export const fetchMovieRequested = (id) => ({
  type: FETCH_MOVIE_REQUESTED,
  payload: { id },
});

export const FETCH_MOVIE_SUCCESSED = 'FETCH_MOVIE_SUCCESSED';
export const fetchMovieSuccessed = (movie) => ({
  type: FETCH_MOVIE_SUCCESSED,
  payload: { movie },
});

export const FETCH_MOVIE_FAILED = 'FETCH_MOVIE_FAILED';
export const fetchMovieFailed = (error) => ({
  type: FETCH_MOVIE_FAILED,
  payload: { error },
});

export const initialState = fromJS({
  isFetching: false,
  didInvalidate: false,
  error: '',
  data: null,
});

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SELECT_MOVIE:
      return state.set('data', fromJS(payload.movie));

    case FETCH_MOVIE_REQUESTED:
      return state.set('isFetching', true).set('didInvalidate', false);

    case FETCH_MOVIE_SUCCESSED:
      return state.set('isFetching', false).set('data', fromJS(payload.movie));

    case FETCH_MOVIE_FAILED:
      return state
        .set('isFetching', false)
        .set('didInvalidate', true)
        .set('error', payload.error);
    default:
      return state;
  }
};
