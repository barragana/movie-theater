import {
  all,
  takeLatest,
  put,
  call,
} from 'redux-saga/effects';

import {
  FETCH_POPULAR_MOVIES_REQUESTED,
  fetchPopularMoviesSuccessed,
  fetchPopularMoviesFailed,
} from '../ducks/popularMovies';
import {
  FETCH_MOVIE_REQUESTED,
  fetchMovieSuccessed,
  fetchMovieFailed,
} from '../ducks/movie';
import {
  searchMoviesSuccessed,
  searchMoviesFailed,
  SEARCH_MOVIES_REQUESTED,
  FILTER_MOVIES_REQUESTED,
} from '../ducks/searchedMovies';

import { getMovie, discoverMovies, searchMovies } from '../api';

export function* handleDiscoverMovies() {
  try {
    const movies = yield call(discoverMovies);
    yield put(fetchPopularMoviesSuccessed(movies.results));
  } catch (e) {
    yield put(fetchPopularMoviesFailed(e));
  }
}

export function* handleGetMovie({ payload: { id } }) {
  try {
    const movie = yield call(getMovie, id);
    yield put(fetchMovieSuccessed(movie));
  } catch (e) {
    yield put(fetchMovieFailed(e));
  }
}

// eslint-disable-next-line consistent-return
export function* handleSearchMovies({ payload: { term } }) {
  try {
    if (!term) {
      return yield put(searchMoviesSuccessed([]));
    }

    const movies = yield call(searchMovies, term);
    yield put(searchMoviesSuccessed(movies.results));
  } catch (e) {
    yield put(searchMoviesFailed(e));
  }
}

export function* handleFilterMovies({ payload: { stars } }) {
  try {
    const maxVotes = stars * 2;
    const minVotes = maxVotes - 2;
    const movies = yield call(
      discoverMovies,
      undefined, { 'vote_average.gte': minVotes, 'vote_average.lte': maxVotes },
    );
    yield put(searchMoviesSuccessed(movies.results));
  } catch (e) {
    yield put(searchMoviesFailed(e));
  }
}

export default function* watchMovies() {
  yield all([
    takeLatest(FETCH_POPULAR_MOVIES_REQUESTED, handleDiscoverMovies),
    takeLatest(FETCH_MOVIE_REQUESTED, handleGetMovie),
    takeLatest(SEARCH_MOVIES_REQUESTED, handleSearchMovies),
    takeLatest(FILTER_MOVIES_REQUESTED, handleFilterMovies),
  ]);
}
