export const sizes = {
  XXXS: 'xxxs',
  XXS: 'xxs',
  XS: 'xs',
  S: 's',
  M: 'm',
  L: 'l',
  XL: 'xl',
  XXL: 'xxl',
  XXXL: 'xxxl',
};

export const centerOptions = {
  V: 'v',
  H: 'h',
  ALL: 'all',
};
