import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`
  html,
  body {
    background-color: ${(props) => props.theme.colors.white};
    margin: 0;
    padding: 0;
  }

  * {
    box-sizing: border-box;
    font-size: 14px;
  }
`;

export default GlobalStyles;
