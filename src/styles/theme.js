import * as borderStyles from './border';

export { default as colors } from './colors';

export const iconSizes = {
  xs: '8px',
  s: '12px',
  m: '16px',
  l: '20px',
  xl: '24px',
  xxl: '32px',
  xxxl: '40px',
};

export const spacing = {
  xxxs: '2px',
  xxs: '4px',
  xs: '8px',
  s: '12px',
  m: '16px',
  l: '20px',
  xl: '24px',
  xxl: '30px',
  xxxl: '36px',
};

export const transitions = {
  presets: {
    default: 'all 0.3s ease-in-out',
  },
};

export const boxShadows = {
  presets: {
    default: '0 5px 7px 0 rgba(0,0,0,0.1)',
  },
};

export const border = borderStyles;
