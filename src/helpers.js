export const mapKeysToLowerCase = (obj) => Object
  .keys(obj)
  .map((key) => key.toLowerCase());

export const reduceKeysToUpperCase = (obj) => Object
  .keys(obj)
  .reduce((res, key) => ({ ...res, [key.toUpperCase()]: key }), {});

export const concatPosterPath = (posterPath) => (posterPath
  ? `${process.env.REACT_APP_POSTER_URL}${posterPath}` : undefined);
