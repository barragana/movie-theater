import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import { sizes } from '../styles/constants';
import { colors } from '../styles/theme';
import { themeAttrPropType } from '../propTypes';
import { reduceKeysToUpperCase } from '../helpers';

const {
  XL, L, M, S, XS,
} = sizes;

const fontStyles = {
  xs: {
    'font-size': '10px',
    'line-height': '14px',
    'letter-spacing': '0.2px',
  },
  s: {
    'font-size': '12px',
    'line-height': '16px',
    'letter-spacing': '0.2px',
  },
  m: {
    'font-size': '14px',
    'line-height': '22px',
  },
  l: {
    'font-size': '16px',
    'line-height': '24px',
  },
  xl: {
    'font-size': '18px',
    'line-height': '24px',
  },
};

const styles = ({
  theme, bold, size, color,
}) => css`
  color: ${theme.colors[color]};
  margin: 0;
  ${() => ({ ...fontStyles[size] })};
  font-weight: ${bold ? 'bold' : 'normal'};
`;
const StyledText = styled.span`
  ${styles};
`;

const Text = (props) => (
  <StyledText {...props} />
);

Text.sizes = {
  XL, L, M, S, XS,
};
Text.colors = reduceKeysToUpperCase(colors);

Text.propTypes = {
  color: themeAttrPropType(colors),
  size: themeAttrPropType(Text.sizes),
  className: PropTypes.string,
  bold: PropTypes.bool,
};

Text.defaultProps = {
  color: Text.colors.N800,
  size: Text.sizes.M,
};

export default Text;
