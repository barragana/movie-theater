import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components/macro';

import { sizes } from '../styles/constants';
import { colors } from '../styles/theme';
import { themeAttrPropType } from '../propTypes';
import { reduceKeysToUpperCase } from '../helpers';

const { L, M, S } = sizes;

const fontStyles = {
  s: {
    'font-size': '18px',
    'line-height': '24px',
    'letter-spacing': '0.1px',
  },
  m: {
    'font-size': '24px',
    'line-height': '32px',
  },
  l: {
    'font-size': '36px',
    'line-height': '40px',
  },
};

const styles = ({
  theme, bold, size, color,
}) => css`
  color: ${theme.colors[color]};
  margin: 0;
  ${() => ({ ...fontStyles[size] })};
  font-weight: ${bold ? 'bold' : 'normal'};
`;
const H1 = styled.h1`
  ${styles};
`;

const Heading = (props) => <H1 {...props} />;

Heading.sizes = { L, M, S };
Heading.colors = reduceKeysToUpperCase(colors);

Heading.propTypes = {
  color: themeAttrPropType(colors),
  size: themeAttrPropType(Heading.sizes),
  className: PropTypes.string,
  bold: PropTypes.bool,
};

Heading.defaultProps = {
  color: Heading.colors.N800,
  size: Heading.sizes.M,
};

export default Heading;
