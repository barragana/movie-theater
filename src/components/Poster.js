import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/macro';

import { withPadding, withMargin } from '../styles/helpers';
import { sizes } from '../styles/constants';

const PosterLink = styled.span`
  cursor: pointer;
  width: 100%;
`;

const Img = styled.img`
  ${withPadding};
`;

const Placeholder = styled.div`
  background-color: ${() => `#${Math.random().toString(16).substr(2, 6)}`};
  opacity: 0.25;
  width: 100%;
  ${withMargin};
`;

const Poster = ({
  className, title, imgPath, padding, onClick,
}) => (!imgPath ? (
  <PosterLink className={className} onClick={onClick}>
    <Placeholder margin={padding} />
  </PosterLink>
) : (
  <PosterLink className={className} onClick={onClick}>
    <Img
      src={imgPath}
      title={title}
      alt={title}
      width="100%"
      padding={padding}
    />
  </PosterLink>
));

Poster.propTypes = {
  title: PropTypes.string,
  imgPath: PropTypes.string,
  className: PropTypes.string,
  padding: PropTypes.string,
  onClick: PropTypes.func,
};
Poster.padding = sizes;

export default Poster;
