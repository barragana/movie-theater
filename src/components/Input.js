import styled from 'styled-components/macro';
import { css } from 'styled-components';

const style = ({ theme }) => css`
  background-color: ${theme.colors.white};
  border: ${theme.border.presets.default};
  border-radius: ${theme.border.borderRadius.m};
  box-shadow: none;
  color: ${theme.colors.n900};
  outline: none;
  padding: ${theme.spacing.xs} ${theme.spacing.s};
  transition: ${theme.transitions.presets.default};
  width: 100%;

  &[disabled] {
    border-color: ${theme.colors.n200};
    &,
    &::placeholder {
      color: ${theme.colors.n300};
    }
  }
  &:focus {
    box-shadow: ${theme.boxShadows.presets.default};
  }
  &::placeholder {
    color: ${theme.colors.n500};
  }
`;

const Input = styled.input`
  ${style}
`;

export default Input;
