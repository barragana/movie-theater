import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/macro';

import popularMoviePropType from '../../propTypes/popularMovies';

import Poster from '../Poster';
import { concatPosterPath } from '../../helpers';

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  height: 100%;
  width: 100%;
`;

const GalleryPoster = styled(Poster)`
  display: flex;
  width: ${({ width }) => width};
`;

const Gallery = ({
  className, movies, onClick, columns,
}) => (
  <Container className={className}>
    {movies.map((movie) => (
      <GalleryPoster
        key={movie.id}
        imgPath={concatPosterPath(movie.poster_path)}
        alt={movie.title}
        id={movie.id}
        padding={Poster.padding.XXXS}
        onClick={() => onClick(movie)}
        width={`${100 / columns}%`}
      />
    ))}
  </Container>
);

Gallery.propTypes = {
  movies: PropTypes.arrayOf(popularMoviePropType).isRequired,
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string,
  columns: PropTypes.number,
};

Gallery.defaultProps = {
  columns: 4,
};

export default Gallery;
