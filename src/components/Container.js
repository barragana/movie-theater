import styled from 'styled-components/macro';

const Container = styled.div`
  width: 95%;
  @media (min-width: 768px) and (max-width: 1024px)  {
    width: 75%;
  }
  @media (min-width: 1024px) {
    width: 50%;
  }
`;

export default Container;
