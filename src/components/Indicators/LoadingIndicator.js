import React from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes, css } from 'styled-components';

import { sizes, centerOptions } from '../../styles/constants';
import {
  withCenter, withCenterV, withCenterH, withCenterAll,
} from '../../styles/helpers';

const Container = styled.div`
  ${withCenter};
  ${withCenterV};
  ${withCenterH};
  ${withCenterAll};
`;

const ellipsis1 = keyframes`
  0% {
    transform: scale(0);
  }
  100% {
    transform: scale(1);
  }
`;

const ellipsis2 = (offset) => keyframes`
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(${offset}, 0);
  }
`;

const ellipsis3 = keyframes`
  0% {
    transform: scale(1);
  }
  100% {
    transform: scale(0);
  };
`;

const style = ({ theme, size, color }) => css`
  display: inline-block;
  position: relative;
  width: calc(${theme.iconSizes[size]} * 6);
  height: ${theme.iconSizes[size]};

  div {
    position: absolute;
    width: ${theme.iconSizes[size]};
    height: ${theme.iconSizes[size]};
    border-radius: 50%;
    background: ${color || theme.colors.n500};
    animation-timing-function: cubic-bezier(0, 1, 1, 0);

    &:nth-child(1) {
      left: 0;
      animation: ${ellipsis1} 0.6s infinite;
    }

    &:nth-child(2) {
      left: 0;
      animation: ${
  ellipsis2(`calc(${theme.iconSizes[size]} * 2.4)`)
} 0.6s infinite;
    }

    &:nth-child(3) {
      left: ${`calc(${theme.iconSizes[size]} * 2.4)`};
      animation: ${
  ellipsis2(`calc(${theme.iconSizes[size]} * 2.4)`)
} 0.6s infinite;
    }

    &:nth-child(4) {
      left: ${`calc(${theme.iconSizes[size]} * 4.8)`};
      animation: ${ellipsis3} 0.6s infinite;
    }
  }
`;

const Wrapper = styled.div`
  ${style}
`;

const LoadingIndicator = ({
  size, color, className, ...props
}) => (
  <Container {...props}>
    <Wrapper className={className} size={size} color={color}>
      <div />
      <div />
      <div />
      <div />
    </Wrapper>
  </Container>
);

LoadingIndicator.sizes = sizes;
LoadingIndicator.centerOptions = centerOptions;
LoadingIndicator.propTypes = {
  size: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
  center: PropTypes.oneOf(Object.keys(centerOptions)
    .map((key) => centerOptions[key])),
};

LoadingIndicator.defaultProps = {
  size: sizes.S,
};

export default LoadingIndicator;
