import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { lighten } from 'polished';

import { centerOptions } from '../../styles/constants';
import { withCenter, withCenterV, withCenterH } from '../../styles/helpers';

const style = ({ theme, center }) => center
  && css`
    background-color: ${lighten(0.25, theme.colors.error)};
    padding: ${theme.spacing.xs} ${theme.spacing.s};
  `;
const Container = styled.div`
  color: ${({ theme }) => theme.colors.error};
  ${style};
  ${withCenter};
  ${withCenterV};
  ${withCenterH};
`;

const ErrorIndicator = ({ children, size, ...props }) => (
  <Container {...props}>
    <span>
      {children}
    </span>
  </Container>
);

ErrorIndicator.centerOptions = centerOptions;
ErrorIndicator.propTypes = {
  size: PropTypes.string,
  className: PropTypes.string,
  center: PropTypes
    .oneOf(Object.keys(centerOptions).map((key) => centerOptions[key])),
  children: PropTypes.node,
};

export default ErrorIndicator;
