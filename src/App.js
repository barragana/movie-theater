import React from 'react';
import styled from 'styled-components/macro';
import { Route, Switch, Redirect } from 'react-router-dom';

import Main from './scenes/Main';
import PosterDetails from './scenes/PosterDetails';
import PopularMovies from './scenes/PopularMovies';

const StyledApp = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  overflow-y: auto;
  padding: 20px 10px;
  position: relative;
  width: 100vw;
`;

function App() {
  return (
    <StyledApp>
      <Switch>
        <Route exact path="/popular-movies" component={PopularMovies} />
        <Route exact path="/:id" component={PosterDetails} />
        <Route exact path="/" component={Main} />
        <Redirect to="/" />
      </Switch>
    </StyledApp>
  );
}

export default App;
