import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router-dom';

import api from './api';

import App from './App';
import GlobalStyles from './styles/global';

import * as theme from './styles/theme';

import store, { history } from './store';

(function inti() {
  api.setBasePath(process.env.REACT_APP_API_URL_V3);
  api.setAuthorization(process.env.REACT_APP_TOKEN_V4);

  ReactDOM.render(
    <React.StrictMode>
      <ThemeProvider theme={theme}>
        <GlobalStyles />
        <Provider store={store}>
          <Router history={history}>
            <Route path="/" component={App} />
          </Router>
        </Provider>
      </ThemeProvider>
    </React.StrictMode>,
    document.getElementById('root'),
  );
}());
