import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components/macro';

import popularMoviePropType from '../propTypes/popularMovies';

import { selectMovie } from '../ducks/movie';
import { fetchPopularMoviesRequested } from '../ducks/popularMovies';
import {
  getPopularMoviesState,
  getPopularMoviesForSection,
  getFirstPopularMovieForSection,
} from '../selectors/movies';

import Poster from '../components/Poster';
import GalleryComponent from '../components/Gallery/Gallery';
import LoadingIndicator from '../components/Indicators/LoadingIndicator';
import ErrorIndicator from '../components/Indicators/ErrorIndicator';
import Container from '../components/Container';
import Text from '../components/Text';

import { concatPosterPath } from '../helpers';
import { history } from '../store';

const GalleryContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const MajorPoster = styled(Poster)`
  align-items: center;
  display: flex;
  height: 100%;
  width: 33.33%;
`;

const Gallery = styled(GalleryComponent)`
  width: 66.66%;
`;

const Header = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  margin-bottom: ${({ theme }) => theme.spacing.xs};
  width: 100%;
`;

const PopularMoviesComponent = ({
  state, requestMovies, movies, firstMovie, setSelectedMovie,
}) => {
  const {
    isFetching, didInvalidate, error,
  } = state;
  useEffect(() => {
    requestMovies();
  }, []);

  if (isFetching) return <LoadingIndicator center={LoadingIndicator.centerOptions.H} />;
  if (didInvalidate) {
    console.log('>>>>>', error);
    return (
      <ErrorIndicator center={ErrorIndicator.centerOptions.H}>
        Something went wrong, please try again.
      </ErrorIndicator>
    );
  }
  if (!movies.length) return false;

  const handlePosterClick = (selectedMovie) => {
    setSelectedMovie(selectedMovie);
    history.push(`/${selectedMovie.id}`);
  };

  return (
    <Container>
      <Header>
        <Text bold>POPULAR MOVIES</Text>
        <Text size={Text.sizes.XS} color={Text.colors.N500}>View All</Text>
      </Header>
      <GalleryContainer>
        {firstMovie && (
          <MajorPoster
            imgPath={concatPosterPath(firstMovie.poster_path)}
            title={firstMovie.title}
            id={firstMovie.id}
            padding={Poster.padding.XXXS}
            onClick={() => { handlePosterClick(firstMovie); }}
          />
        )}
        <Gallery movies={movies} onClick={handlePosterClick} />
      </GalleryContainer>
    </Container>
  );
};

PopularMoviesComponent.propTypes = {
  requestMovies: PropTypes.func.isRequired,
  setSelectedMovie: PropTypes.func.isRequired,
  state: PropTypes.shape({
    isFetching: PropTypes.bool,
    didInvalidate: PropTypes.bool,
    error: PropTypes.any,
  }).isRequired,
  movies: PropTypes.arrayOf(popularMoviePropType).isRequired,
  firstMovie: popularMoviePropType,
};

PopularMoviesComponent.defaultProps = {
  firstMovie: null,
};

const mapStateToProps = (state) => ({
  state: getPopularMoviesState(state),
  movies: getPopularMoviesForSection(state),
  firstMovie: getFirstPopularMovieForSection(state),
});

const mapDispatchToProps = {
  requestMovies: fetchPopularMoviesRequested,
  setSelectedMovie: selectMovie,
};

const PopularMovies = connect(
  mapStateToProps,
  mapDispatchToProps,
)(PopularMoviesComponent);

export default PopularMovies;
