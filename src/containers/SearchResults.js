import React from 'react';
import styled from 'styled-components/macro';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { history } from '../store';
import popularMoviePropType from '../propTypes/popularMovies';

import LoadingIndicator from '../components/Indicators/LoadingIndicator';
import ErrorIndicator from '../components/Indicators/ErrorIndicator';
import Gallery from '../components/Gallery/Gallery';
import Container from '../components/Container';
import Heading from '../components/Heading';
import Stars from '../components/Stars';

import {
  getSearchedMoviesState,
  getSearchedMovies,
  getStarsFilter,
} from '../selectors/movies';
import { selectMovie } from '../ducks/movie';
import { filterMoviesRequested } from '../ducks/searchedMovies';

const StyledSearchResultContainer = styled(Container)`
  height: 100%;
  margin: ${({ theme }) => theme.spacing.xl} 0;
  @media (min-width: 1024px) {
    width: 75%;
  }
`;

const Header = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  margin-bottom: ${({ theme }) => theme.spacing.m};
  width: 100%;
`;

const SearchResults = ({
  movies, state, onPosterClick,
}) => {
  const {
    isFetching, didInvalidate, error,
  } = state;

  if (isFetching) {
    return <LoadingIndicator center={LoadingIndicator.centerOptions.H} />;
  }
  if (didInvalidate) {
    console.log('>>>>>', error);
    return (
      <ErrorIndicator center={ErrorIndicator.centerOptions.H}>
        Something went wrong, please try again.
      </ErrorIndicator>
    );
  }
  if (!movies.length) return false;

  return (
    <Gallery movies={movies} onClick={onPosterClick} columns={10} />
  );
};

SearchResults.propTypes = {
  state: PropTypes.shape({
    isFetching: PropTypes.bool,
    didInvalidate: PropTypes.bool,
    error: PropTypes.any,
  }).isRequired,
  movies: PropTypes.arrayOf(popularMoviePropType).isRequired,
  onPosterClick: PropTypes.func.isRequired,
};

const SearchResultsContainerComponent = ({
  state,
  movies,
  setSelectedMovie,
  filterMovies,
  stars,
}) => {
  const handlePosterClick = (selectedMovie) => {
    setSelectedMovie(selectedMovie);
    history.push(`/${selectedMovie.id}`);
  };

  return (
    <StyledSearchResultContainer>
      <Header>
        <Heading bold size={Heading.sizes.S}>
          {(!!movies.length && 'Results') || 'Filter'}
        </Heading>
        <Stars onClick={filterMovies} stars={stars} />
      </Header>
      <SearchResults
        movies={movies}
        onPosterClick={handlePosterClick}
        state={state}
        stars={stars}
      />
    </StyledSearchResultContainer>
  );
};

SearchResultsContainerComponent.propTypes = {
  setSelectedMovie: PropTypes.func.isRequired,
  filterMovies: PropTypes.func.isRequired,
  state: SearchResults.propTypes.state,
  movies: SearchResults.propTypes.movies,
  stars: PropTypes.number.isRequired,
};


const mapStateToProps = (state) => ({
  state: getSearchedMoviesState(state),
  movies: getSearchedMovies(state),
  stars: getStarsFilter(state),
});

const mapDispatchToProps = {
  setSelectedMovie: selectMovie,
  filterMovies: filterMoviesRequested,
};

const SearchResultsContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchResultsContainerComponent);

export default SearchResultsContainer;
