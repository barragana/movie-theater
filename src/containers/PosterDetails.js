import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components/macro';
import { useParams, Link } from 'react-router-dom';

import popularMoviePropType from '../propTypes/popularMovies';


import Poster from '../components/Poster';
import Container from '../components/Container';
import Text from '../components/Text';
import LoadingIndicator from '../components/Indicators/LoadingIndicator';

import { concatPosterPath } from '../helpers';
import { fetchMovieRequested } from '../ducks/movie';
import { getSelectedMovie } from '../selectors/movies';
import Stars from '../components/Stars';

const Wrapper = styled(Container)`
  display: flex;
  flex-wrap: wrap;
`;

const MajorPoster = styled(Poster)`
  align-items: center;
  display: flex;
  height: 100%;
  width: 33.33%;
`;

const Header = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  margin-bottom: ${({ theme }) => theme.spacing.xs};
  width: 100%;
`;

const DetailsContainer = styled.div`
  align-items: flex-start;
  display: flex;
  flex-wrap: wrap;
  height: 100%;
  padding-left: ${({ theme }) => theme.spacing.m};
  width: 66.67%;
`;

const Label = styled(Text)`
  flex-shrink: 0;
  width: 70px;
`;

const Row = styled.div`
  align-items: center;
  display: flex;
  margin-bottom: ${({ theme }) => theme.spacing.s};
  width: 100%;
`;

const PosterDetails = ({ requestMovie, movie }) => {
  const { id } = useParams();
  useEffect(() => {
    requestMovie(id);
  }, []);

  if (id && !movie) {
    return (
      <LoadingIndicator center={LoadingIndicator.centerOptions.ALL} />
    );
  }

  return (
    <Wrapper>
      <Header>
        <Link to="/"><Text bold size={Text.sizes.XL}>Home</Text></Link>
      </Header>
      <Header>
        <Text bold size={Text.sizes.XL}>{movie.title}</Text>
        <Stars stars={movie.vote_average} />
      </Header>
      <MajorPoster
        imgPath={concatPosterPath(movie.poster_path)}
        title={movie.title}
        id={movie.id}
      />
      <DetailsContainer>
        <Row>
          <Label size={Text.sizes.S} color={Text.colors.N500}>
            Overview:
          </Label>
          <Text size={Text.sizes.M}>{movie.overview}</Text>
        </Row>
        <Row>
          <Label size={Text.sizes.S} color={Text.colors.N500}>
            Release:
          </Label>
          <Text size={Text.sizes.M}>{movie.release_date}</Text>
        </Row>
        <Row>
          <Label size={Text.sizes.S} color={Text.colors.N500}>
            Language:
          </Label>
          <Text size={Text.sizes.M}>
            {movie.original_language.toUpperCase()}
          </Text>
        </Row>
        {movie.homepage && (
          <Row>
            <Label size={Text.sizes.S} color={Text.colors.N500}>
              Site:
            </Label>
            <Text size={Text.sizes.M}>
              <a href={movie.homepage}>Homepage</a>
            </Text>
          </Row>
        )}
        {movie.genres && (
          <Row>
            <Label size={Text.sizes.S} color={Text.colors.N500}>
              Genders:
            </Label>
            <Text size={Text.sizes.M}>
              {movie.genres.map((gender) => gender.name).join(', ')}
            </Text>
          </Row>
        )}
      </DetailsContainer>
    </Wrapper>
  );
};

PosterDetails.propTypes = {
  requestMovie: PropTypes.func.isRequired,
  movie: popularMoviePropType,
};

const mapStateToProps = (state) => ({
  movie: getSelectedMovie(state),
});

const mapDispatchToProps = {
  requestMovie: fetchMovieRequested,
};

const PopularMovies = connect(
  mapStateToProps,
  mapDispatchToProps,
)(PosterDetails);

export default PopularMovies;
