import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components/macro';

import Input from '../components/Input';
import Heading from '../components/Heading';

import { selectMovie } from '../ducks/movie';
import { searchMoviesRequested } from '../ducks/searchedMovies';
import { getSearchTerm } from '../selectors/movies';

const Img = styled.img`
  border-top-left-radius: 25px;
  border-top-right-radius: 25px;
  object-fit: cover;
`;

const SearchSectionContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  position: relative;
  margin-bottom: ${({ theme }) => theme.spacing.l};
  width: 95%;
`;

const SearchContainer = styled.div`
  align-items: center;
  display: flex;
  flex-wrap: wrap;
  height: 200px;
  justify-content: center;
  position: absolute;
  top: 100px;
  width: 100%;
`;

const SearchInput = styled(Input)`
  width: 80%;
  @media (min-width: 768px) and (max-width: 1024px)  {
    width: 60%;
  }
  @media (min-width: 1024px) {
    width: 45%;
  }
`;

const Header = styled.div`
  margin-bottom: ${({ theme }) => theme.spacing.l};
  text-align: center;
  width: 100%;
`;

const SearchMoviesComponent = ({ searchMovies, term }) => {
  const [searchValue, changeSearch] = useState(term);
  const searchTimeout = useRef();

  const handleSearchChange = (e) => {
    const { value } = e.target;
    changeSearch(value);
    clearTimeout(searchTimeout.current);
    searchTimeout.current = setTimeout(
      () => searchMovies(value),
      600,
    );
  };

  useEffect(() => {
    if (term !== searchValue) { changeSearch(term); }
  }, [term]);

  return (
    <>
      <SearchSectionContainer>
        <Img
          src="https://images.unsplash.com/photo-1568876694728-451bbf694b83"
          alt="Movie section"
          height="400"
          width="100%"
        />
        <SearchContainer>
          <Header>
            <Heading size={Heading.sizes.L} color={Heading.colors.WHITE} bold>
              Your favorites movies in one place.
            </Heading>
          </Header>
          <SearchInput value={searchValue} onChange={handleSearchChange} />
        </SearchContainer>
      </SearchSectionContainer>
    </>
  );
};

SearchMoviesComponent.propTypes = {
  searchMovies: PropTypes.func.isRequired,
  term: PropTypes.string,
};


const mapStateToProps = (state) => ({
  term: getSearchTerm(state),
});

const mapDispatchToProps = {
  searchMovies: searchMoviesRequested,
  setSelectedMovie: selectMovie,
};

const SearchMovies = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchMoviesComponent);

export default SearchMovies;
