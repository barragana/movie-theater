import qs from 'qs';
import { sortBy } from '../constants';

function api() {
  function getHeaders() {
    return {
      'content-type': 'application/json',
      Authorization: `Bearer ${api.AUTHORIZATION}`,
    };
  }

  function getBody(data) {
    return JSON.stringify(data);
  }

  function getOptions({ method, data } = { method: 'GET' }) {
    return {
      method,
      body: getBody(data),
      headers: getHeaders(),
    };
  }

  function getURL(path) {
    return path ? `${api.BASE_PATH}${path}` : api.BASE_PATH;
  }

  function getQueryString({ queryString } = {}) {
    if (!queryString) return '';

    const qString = qs.stringify(queryString, { arrayFormat: 'repeat' });
    return `?${qString}`;
  }

  async function request(url, options) {
    const fullURL = `${getURL(url)}${getQueryString(options)}`;
    const parsedOptions = getOptions(options);
    const res = await fetch(`${fullURL}`, parsedOptions);
    const result = await res.json();
    if (!res.ok) {
      const error = new Error(res.statusText);
      error.status = res.status;
      error.body = result;
      throw error;
    }
    return result;
  }

  return request;
}

api.setBasePath = (basePath) => { api.BASE_PATH = basePath; };
api.setAuthorization = (auth) => { api.AUTHORIZATION = auth; };

export const request = api();

export const discoverMovies = (
  sort_by = sortBy.popularity.desc,
  query = {},
  page = 1,
) => request('/discover/movie', { queryString: { ...query, page, sort_by } });

export const searchMovies = (
  query,
  page = 1,
) => request('/search/movie', { queryString: { query, page } });

export const getMovie = (id) => request(`/movie/${id}`);

export default api;
