export const sortBy = {
  popularity: {
    ASC: 'popularity.asc',
    DESC: 'popularity.desc',
  },
};
