import React from 'react';
import { useParams } from 'react-router-dom';

import { history } from '../store';

import PosterDetailsContainer from '../containers/PosterDetails';

const PosterDetails = () => {
  const { id } = useParams();
  if (!parseInt(id, 10)) {
    history.replace('/');
    return false;
  }

  return (
    <PosterDetailsContainer />
  );
};

export default PosterDetails;
