import React from 'react';
import styled from 'styled-components/macro';

import PopularMovies from '../containers/PopularMovies';
import SearchMovies from '../containers/SearchMovies';
import SearchResults from '../containers/SearchResults';

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

const Main = () => (
  <Container>
    <SearchMovies />
    <SearchResults />
    <PopularMovies />
  </Container>
);

export default Main;
