import PropTypes from 'prop-types';
import datePropType from './date';

const popularMovies = {
  popularity: PropTypes.number.isRequired,
  vote_count: PropTypes.number.isRequired,
  video: false,
  poster_path: PropTypes.string,
  id: PropTypes.number.isRequired,
  adult: false,
  backdrop_path: PropTypes.string,
  original_language: PropTypes.string.isRequired,
  original_title: PropTypes.string.isRequired,
  genre_ids: PropTypes.arrayOf(PropTypes.number),
  title: PropTypes.string,
  vote_average: PropTypes.number,
  overview: PropTypes.string.isRequired,
  release_date: datePropType,
};

const popularMoviePropType = PropTypes.shape(popularMovies);
export default popularMoviePropType;
