import PropTypes from 'prop-types';
import { mapKeysToLowerCase } from '../helpers';

export const themeAttrPropType = (attrs) => PropTypes
  .oneOf(mapKeysToLowerCase(attrs));
