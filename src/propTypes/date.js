const DATE_REGEX = new RegExp(
  /^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/,
);

function datePropValidation({ propName: propValue }, propName, componentName) {
  if (propValue && !DATE_REGEX.test(propValue)) {
    return new Error(`Invalid prop ${
      propName
    } of ${propValue} supplied to ${componentName}. Validation failed.`);
  }
  return null;
}

const datePropType = datePropValidation;
export default datePropType;
