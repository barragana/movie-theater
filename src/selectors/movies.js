export const getPopularMoviesState = (state) => state
  .popularMovies
  .remove('data')
  .toJS();

export const getFirstPopularMovieForSection = (state) => {
  const movies = state.popularMovies.getIn(['data', 0]);
  if (!movies) return movies;
  return movies.toJS();
};

export const getPopularMoviesForSection = (state) => {
  const movies = state.popularMovies.get('data');
  if (!movies.size) return [];
  return movies
    .skip(1)
    .skipLast(11)
    .toJS();
};

export const getSelectedMovie = (state) => {
  const movie = state.movie.get('data');
  if (!movie) return null;
  return movie.toJS();
};

export const getSearchTerm = (state) => state.searchedMovies.get('term');

export const getSearchedMoviesState = (state) => state
  .searchedMovies
  .remove('data')
  .toJS();

export const getSearchedMovies = (state) => {
  const movies = state.searchedMovies.get('data');
  if (!movies) return movies;
  return movies.toJS();
};

export const getStarsFilter = (state) => state.searchedMovies.get('stars');
